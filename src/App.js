import React, { Component } from 'react';
import './App.css';

//third party
import axios from 'axios';

import './api/api';
import Header from './components/global/Header';
import Dashboard from './components/dashboard/Dashboard';
import API from './api/api';

class App extends Component {

  api;
  apiData = [];

  state = {
    movieData: [],
    genres: [],
    category: 'popular'
  }

  componentDidMount() {
      this.api = new API();
      this.page = 1;
      this.fetchData({key: 'genres', callback: this.getGenres});
      this.fetchData({key:'category', category:'popular', callback: this.printThumbData});
      //this.fetchData2('https://api.themoviedb.org/3/genre/movie/list?api_key=f322fc7cdadadced1c647ad778f73fbe&language=en-US', this.getGenres);
      window.addEventListener('scroll', (e) => {
        const scrollPosition = document.scrollingElement.scrollTop + document.scrollingElement.clientHeight;
        const targetScrollPosition = document.body.scrollHeight;
        if (scrollPosition >= targetScrollPosition) {
          console.log('scroll');
          // console.log('Fetch new data');
          this.page += 1;
          console.log(this.page);
          this.fetchData({key: 'category', category: this.state.category, callback: this.printThumbData});
        }
      })
  }

  fetchData2 = (endPoint, callback) => {
    axios.get(endPoint)
      .then((response) => {
          console.log('Genres data: ', response.data);
          console.log(response.data.genres);
          return callback(response.data.genres);
          // for(const item of response.data.results) {
          //     let posterImage;
          //     item.poster_path !== null ? posterImage = `${posterURL}${item.poster_path}`
          //                               : posterImage ='https://via.placeholder.com/154x234';
          //     this.setState({
          //       movieData: [
          //         ...this.state.movieData,
          //         {
          //           id: item.id,
          //           title: item.title,
          //           rating: item.vote_average,
          //           image: `${posterImage}`,
          //           overview: item.overview
          //         }
          //       ]
          //     })
          // }
    }).catch((error) => {
        console.log(error);
    });
  };

  getGenres = (response) => {
    console.log('got the genres');
    for(let item of response.data.genres) {
      this.setState({
        genres: [...this.state.genres, item]
      });
    }
  };

  fetchData = ({key, category, query, callback}) => {
    console.log('Entra qui');
    const data = this.api.fetchData(key, category, query, this.page);
    data.then((response) => {
      console.log(response);
      this.setState({
        category: category
      })
      callback(response);
    }).catch((error) => {
      console.log(error);
    });
  };

  fetchCategory = (category, type) => {
   console.log(this.api.getEndPoint(type));
    const posterURL = 'https://image.tmdb.org/t/p/w154';
    this.api.fetchCategories(category, this.page)
    .then((response) => {
      console.log('My response within App', response);
      for(const item of response.data.results) {
        let posterImage;
        item.poster_path !== null ? posterImage = `${posterURL}${item.poster_path}`
                                  : posterImage ='https://via.placeholder.com/154x234';
        this.setState({
          movieData: [
            ...this.state.movieData,
            {
              id: item.id,
              title: item.title,
              rating: item.vote_average,
              image: `${posterImage}`,
              overview: item.overview
            },
          ],
          category: category
        })
      }
    }).catch((error) => {
      console.log(error);
    });
    // const endPointMovieDB = search ? `https://api.themoviedb.org/3/search/movie?api_key=f322fc7cdadadced1c647ad778f73fbe&query=${search}&include_adult=false`
    //                                : `https://api.themoviedb.org/3/movie/${category}?page=${page}&api_key=f322fc7cdadadced1c647ad778f73fbe`;
    // const posterURL = 'https://image.tmdb.org/t/p/w154';
    // axios.get(endPointMovieDB)
    //   .then((response) => {
    //       console.log(response);
    //       for(const item of response.data.results) {
    //           let posterImage;
    //           item.poster_path !== null ? posterImage = `${posterURL}${item.poster_path}`
    //                                     : posterImage ='https://via.placeholder.com/154x234';
    //           this.setState({
    //             movieData: [
    //               ...this.state.movieData,
    //               {
    //                 id: item.id,
    //                 title: item.title,
    //                 rating: item.vote_average,
    //                 image: `${posterImage}`,
    //                 overview: item.overview
    //               }
    //             ]
    //           })
    //       }
    // }).catch((error) => {
    //     console.log(error);
    // });
  }

  formatThumbData = (response) => {
    const posterURL = 'https://image.tmdb.org/t/p/w154';
    for(const item of response.data.results) {
      let posterImage;
      item.poster_path !== null ? posterImage = `${posterURL}${item.poster_path}`
                                  : posterImage ='https://via.placeholder.com/154x234';
      this.apiData.push({
        id: item.id,
        title: item.title,
        rating: item.vote_average,
        image: `${posterImage}`,
        overview: item.overview
      });
    }
    return this.apiData;
  };

  printThumbData = (response) => {
    this.formatThumbData(response);
    this.setState({
      movieData: this.apiData
    })
  };

  // printCategoryData = (response) => {
  //   console.log('Hola');
  //   console.log('FROM CATEGORY DATA', response);
  //   this.apiData = [];
  //   this.formatThumbData(response);
  //   this.setState({
  //     movieData: this.apiData
  //   })
  // };

  // printSearchData = (response) => {
  //   console.log('Mi data', response)
  //   this.apiData = [];
  //   this.formatThumbData(response);
  //   this.setState({
  //     movieData: this.apiData
  //   })
  // };

  clearMovieData = (category) => {
    this.page = 1;
    this.apiData = [];
    this.setState({
      movieData: []
    });
    //this.fetchCategory(category);
  };

  render() {
    return (
      <div className="App">
        <Header fetchData={this.fetchData}
                clearMovieData={this.clearMovieData}
                genres={this.state.genres}
                printThumbData={this.printThumbData} />
        <Dashboard data={this.state.movieData} />
      </div>
    );
  }
}

export default App;
