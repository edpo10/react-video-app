import React from 'react';
import './MovieRating.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStarHalfAlt } from '@fortawesome/free-solid-svg-icons';
import { faStar } from '@fortawesome/free-solid-svg-icons';

class MovieRating extends React.Component {

    state = {
        starsArray: []
    }

    componentDidMount() {
       this.formatRating();
    };

    formatRating = () => {
        const starsArray = [];
        const starRating = (this.props.rating / 10) * 5;
        const fixedRating = Math.floor(starRating);
        const numToStr = this.props.rating.toString();
        const strIndex = numToStr.indexOf('.');
        const decimal =  numToStr.slice(strIndex + 1);

        for (let index = 0; index < fixedRating; index++) {
            starsArray.push(<FontAwesomeIcon icon={faStar} size="3x"/>);
        }

        if(strIndex !== -1 && decimal >= 5) {
            starsArray.push(<FontAwesomeIcon icon={faStarHalfAlt} size="3x"/>);
        }

        this.setState({
            starsArray: starsArray
        });
    }


    render() {
        return (
            <div className="movie-rating">
                {
                    //safe to use index in here
                    this.state.starsArray.map((star, index) => <span key={index}>{star}</span>)
                }
            </div>
        )
    }
}

export default  MovieRating;