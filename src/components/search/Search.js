import React from 'react';

class Search extends React.Component {
    render() {
        return (
            <section>
                <form action="">
                    <input type="text" name="" id="" onChange={this.props.handleSearchChange}/>
                    <a href="#"><span>&#128270;</span></a>
                </form>
            </section>
        );
    }
}

export default Search;