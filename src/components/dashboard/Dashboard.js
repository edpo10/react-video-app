import React from 'react';
import Thumb from './Thumb/Thumb';

//styles
import './Dashboard.css';

class Dashboard extends React.Component {
    render() {
        return (
            this.props.data ?
            <section className="dashboard">
                {
                    this.props.data.map(item =>
                        <Thumb key={item.id}
                               movieID={item.id}
                               title={item.title}
                               rating={item.rating}
                               image={item.image}
                               overview={item.overview} />)
                }
            </section>
            : null
        )
    }
}

export default Dashboard;