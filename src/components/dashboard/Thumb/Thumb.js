import React from 'react';
import './Thumb.css';
import Modal from '../../Modal';
import MovieName from '../../MovieName';
import MoviePoster from '../../MoviePoster';
import MovieRating from '../../MovieRating';

import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay } from '@fortawesome/free-solid-svg-icons';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';

class Thumb extends React.Component {
    state = {
        toogleModal: false,
        youtubeKey: null
    }
    handleShowTrailer = event => {
        event.preventDefault();

        const endPointMovieTrailer = `https://api.themoviedb.org/3/movie/${this.props.movieID}/videos?api_key=f322fc7cdadadced1c647ad778f73fbe&language=en-US`;
        /* Movie details with trailer */
        //const endPointMovieTrailer = `https://api.themoviedb.org/3/movie/${this.props.movieID}?language=en-US&append_to_response=videos&api_key=f322fc7cdadadced1c647ad778f73fbe`;

        axios.get(endPointMovieTrailer)
        .then((response) => {
            console.log(response);
            this.setState({
                toogleModal: !this.state.toogleModal,
                youtubeKey: response.data.results[0].key
            });
        }).catch((error) => {
            console.log(error);
        });
    }
    render() {
        return (
            <div className="video-thumb">
                <MovieName name={this.props.title} />
                <MoviePoster
                    image={this.props.image}
                    alt={this.props.title}
                    handleShowTrailer={this.handleShowTrailer}/>
                    {<span className="video-thumb__info" onClick={this.handleShowTrailer}><FontAwesomeIcon icon={faInfoCircle} /></span>}
                {/*<p>{this.props.rating}</p>*/}
                {/*<MovieName title={this.props.title}/>*/}
                {/*<a className="play-btn" href="#"><FontAwesomeIcon icon={faPlay} /></a>*/}
                {
                    this.state.toogleModal ?
                        <Modal containerClass="">
                            <div className="movieInfoTrailer">
                                <a href="#">&#x2716;</a>
                                <iframe allowFullScreen="allowFullScreen" width="640" height="360" src={`https://www.youtube.com/embed/${this.state.youtubeKey}`}></iframe>
                                <MovieRating rating={this.props.rating} />
                            </div>
                            <div className="movieInfoDetails">
                                <h1>{this.props.title}</h1>
                                <p>{this.props.overview}</p>
                            </div>
                        </Modal>
                    : null
                }
            </div>
        )
    }
}

export default Thumb;