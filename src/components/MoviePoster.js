import React from 'react';

class MoviePoster extends React.Component {
    render() {
        return(
            <a  href="#" className="video-thumb__img" onClick={this.props.handleShowTrailer}>
                <img src={this.props.image} alt={this.props.alt} />
            </a>
        );
    }
}

export default MoviePoster;