import React from 'react';

class MovieName extends React.Component {
    render() {
        return(
            <p className="video-title">{this.props.title}</p>
        );
    }
}

export default MovieName;