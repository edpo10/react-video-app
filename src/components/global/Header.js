import React from 'react';

import Search from '../search/Search';
import './Header.css';

import logo from '../../placeholder-logo.png';

class Header extends React.Component {
    handlePopular = (event) => {
        event.preventDefault();
        console.log('handlePopular');
        this.props.clearMovieData('popular');
        this.props.fetchData({key: 'category', category: 'popular', callback: this.props.printThumbData});
    };

    handleLatest = (event) => {
        event.preventDefault();
        console.log('handleUpComing');
        this.props.clearMovieData('upcoming');;
        this.props.fetchData({key: 'category', category: 'upcoming', callback: this.props.printThumbData});
    };

    handleTopRated = (event) => {
        event.preventDefault();
        console.log('handleTopRated');
        this.props.clearMovieData('top_rated');
        this.props.fetchData({key: 'category', category: 'top_rated', callback: this.props.printThumbData});
    };

    handleSearchChange = (event) => {
        console.log(event.target.value);
        const inputValue = event.target.value;
        this.props.clearMovieData();
        this.props.fetchData({key: 'search', query: inputValue, callback: this.props.printThumbData});
    };

    handleGenre = (event, genre) => {
        event.preventDefault();
        console.log(genre);
        //this.props.fetchData(genre);
    };

    render() {
        return (
            <header className="app-header">
                <a href="#">
                    <img src={logo} className="App-logo" alt="logo" />
                </a>
                <nav>
                    <ul className="app-header__nav">
                        <li><a href="#" onClick={this.handleLatest}>Upcoming</a></li>
                        <li><a href="#" onClick={this.handlePopular}>Popular</a></li>
                        <li><a href="#" onClick={this.handleTopRated}>Top Rated</a></li>
                        <li>
                            <a href="#">Genres</a>
                            <ul className="app-header__genres">
                                {
                                    this.props.genres.map(genre => <li key={genre.id}><a href="#" onClick={(e) => this.handleGenre(e, genre.name)}>{genre.name}</a></li>)
                                }
                            </ul>
                        </li>
                    </ul>
                </nav>
                <Search handleSearchChange={this.handleSearchChange}/>
            </header>
        )
    }
}

export default Header;