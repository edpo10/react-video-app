import axios from 'axios';

const API_KEY = 'f322fc7cdadadced1c647ad778f73fbe';

class API {

    fetchCategories(type, page) {
        const endPoint = this.getEndPoint(type, null, page);
        return this.getApiData(endPoint.categories);
    }

    fetchSearch() {

    }

    getEndPoint(category, query, page) {
        return {
            general: 'general Endpoint',
            search: `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&query=${query}&include_adult=false`,
            category: `https://api.themoviedb.org/3/movie/${category}?page=${page}&api_key=${API_KEY}`,
            genres: `https://api.themoviedb.org/3/genre/movie/list?api_key=${API_KEY}&language=en-US`
        };
    }

    fetchData(key, category, query, page) {
        const endPoint = this.getEndPoint(category, query, page)[key];
        return this.getApiData(endPoint);
    }
    // fetchData(endPoint, type, page) {
    //     axios.get(endPoint)
    //     .then((response) => {
    //       console.log(response.data);
    //     }).catch((error) => {
    //         console.log(error);
    //     });
    // }

    getApiData(endPoint) {
        return axios.get(endPoint);
    }
}

export default API;